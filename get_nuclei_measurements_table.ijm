#@File dir(label="Select an input directory", style="directory")
#@String fileExtension(label="Image file extension (e.g. .tif)", value=".tif")
#@int dapiChannel(label="DAPI channel", value=1)
#@int conditionChannel(label="Condition channel", value=3)
#@int radius(label="Filter radius", value=0)


function openImage(filePath) {
	
	if (fileExtension==".tif") {
		open(filePath);
	} else {
		run("Bio-Formats Importer", "open=[" + filePath + "]");
	}

	if (nImages == 0) {
	    print("Failed to open image: " + filePath);
	    exit();
	}
}


//function chooseChannelWindow(color) {
//    list = getList("image.titles");
//    for (i = 0; i < list.length; i++) {
//        if (endsWith(list[i], "(" + color + ")")) {
//            selectWindow(list[i]);
//        }
//    }
//}


function chooseChannelWindow(color_idx) {
    list = getList("image.titles");
    for (i = 0; i < list.length; i++) {
        if (startsWith(list[i], "C" + color_idx)) {
            selectWindow(list[i]);
        }
    }
}


function getAndSaveBackgroundMeasurements(title, series) {
	
	// Get the mask from the LabelImage
	selectWindow("LabelImage_" + title + '_' + series + ".tif");
	setAutoThreshold("Default dark");
	wait(500);
	getMinAndMax(min, max);
	setThreshold(0.5, max);
	wait(1000);
	run("Convert to Mask");
	run("Invert");
	run("Create Selection");
	
	// Set the mask to the condition image (AKA green)
	chooseChannelWindow(conditionChannel);
	run("Restore Selection");
	//Run measurements and save
	run("Measure");
	selectWindow("Results");
	
	title_condition = getTitle();
	
	saveAs("Results", "" + analysisDir + File.separator + "BackgroundResults_" + title_condition + "_" + series + ".csv");
}


function runImageFile(dirPath, imName) {
	
	filePath = "" + dirPath + File.separator + imName;
    
    Ext.setId(filePath);
    Ext.getSeriesCount(seriesCount);

    // Iterate through series if more than one is present
    for (series = 0; series < seriesCount; series++) {

        Ext.setSeries(series);
        Ext.openImagePlus(filePath);

        processImage(dirPath, imName, series);
        
        // Close the current image after processing
        if (nImages > 0) {
            close();
        }
    }

    if (nImages == 0) {
        print("Failed to open image: " + filePath);
        // Remove exit() to continue processing other files even if one fails
    }
}


// Find all files in dir:
function walkFiles(dir) {
	list = getFileList(dir);
	for (i=0; i<list.length; i++) {
		if (endsWith(list[i], "/"))
		   walkFiles(""+dir+list[i]);

		// If image file
		else  if (endsWith(list[i], fileExtension)) {
		   //processImage(dir, list[i]);
		   runImageFile(dir, list[i]);
		   
		   
		}
	}
}


function processImage(dirPath, imName, series) {
	
	//filePath = "" + dirPath + File.separator + imName;
	
	//openImage(filePath);
	
	// Split the channels
	run("Split Channels");
	

	chooseChannelWindow(dapiChannel);

	// Inflate nuclei:
	run("Maximum...", "radius=" + radius);
	
	
	title = getTitle();
	wait(500);
	// Run Stardist - Segmentation of each nucleus object as a different label
	run("Command From Macro", "command=[de.csbdresden.stardist.StarDist2D], args=['input':'" + title + "', " +
	"'modelChoice':'Versatile (fluorescent nuclei)', 'normalizeInput':'true', 'percentileBottom':'1.0', 'percentileTop':'99.8', " +
	"'probThresh':'0.5', 'nmsThresh':'0.8', 'outputType':'Both', 'nTiles':'8', 'excludeBoundary':'2', 'roiPosition':'Automatic', " +
	"'verbose':'false', 'showCsbdeepProgress':'false', 'showProbAndDist':'false'], process=[false]");
	wait(500);
	selectImage("Label Image"); // That's the image results from StarDist segmentation
	
	saveAs("Tiff", "" + analysisDir + File.separator + "LabelImage_" + title + '_' + series);

	// Get measurements of each nucleus and save to csv 
	chooseChannelWindow(conditionChannel);
	
	title_condition = getTitle();
	
	roiManager("Deselect");
	roiManager("multi-measure measure_all");
	saveAs("Results", "" + analysisDir + File.separator + "Results_" + title_condition + '_' + series + ".csv");
	
	// Clear all selections and measurements
	roiManager("Delete");
	close("ROI Manager");
	if (isOpen("Results")) {
		selectWindow("Results");
    	run("Close");
	}
	
	getAndSaveBackgroundMeasurements(title, series);
	
	// Close table:
	if (isOpen("Results")) {
		selectWindow("Results");
    	run("Close");
	}
	// Close all image windows:
	run("Close All");	
	while (nImages>0) { 
		selectImage(nImages); 
		close(); 
    } 
}
	

run("Bio-Formats Macro Extensions");
// Specify the relevant analysis channels
//colorDapi = "blue";
//colorCondition = "green";

// Set the type of measurements to be saved to csv
run("Set Measurements...", "area mean standard min redirect=None decimal=3");

// Create an analysis dir where all output will be saved
analysisDir = dir + File.separator + "brightness_analysis_R" + radius;
File.makeDirectory(analysisDir);

walkFiles(dir);



